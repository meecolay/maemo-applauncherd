/***************************************************************************
**
** Copyright (C) 2012 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (directui@nokia.com)
**
** This file is part of applauncherd
**
** If you have questions regarding the use of this file, please contact
** Nokia at directui@nokia.com.
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License version 2.1 as published by the Free Software Foundation
** and appearing in the file LICENSE.LGPL included in the packaging
** of this file.
**
****************************************************************************/

#ifndef WHITELIST_H
#define WHITELIST_H

const char * const app_whitelist[] =
{
    "/usr/bin/conndlgs",
    "/usr/bin/call-ui",
    "/usr/bin/accounts-ui",
    "/usr/bin/grob",
    "/usr/bin/calc",
    "/usr/bin/organiser",
    "/usr/bin/call-history",
    "/usr/bin/camera-ui",
    "/usr/bin/contacts",
    "/usr/bin/duicontrolpanel.launch",
    "/usr/bin/gallery",
    "/usr/bin/fenix",
    "/usr/bin/mediaviewer",
    "/usr/bin/messaging-ui",
    "/usr/bin/music-suite",
    "/usr/bin/notes",
    "/usr/bin/office-tools",
    "/usr/bin/search",
    "/usr/bin/sync-ui",
    "/usr/bin/userguide",
    "/usr/bin/video-suite",
    "/usr/bin/videosheetplayer"
};

#endif // WHITELIST_H
